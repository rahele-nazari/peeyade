import React from "react";
import { Input } from "antd";
import { request } from "../request/request";
import { Consumer } from "./storeContext";
import { phoneNumberPost } from "../request/services";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: null
    };
  }

  handlePhoneNumber = e => {
    this.setState({
      phone: e.target.value
    });
  };

  onSubmit = async context => {
    const { phone } = this.state;
    const body = {
      phone
    };
    const resp = await request.post(phoneNumberPost, body);
    console.log("resp:", resp);
    context.changeOtp(resp.data);
  };

  render() {
    const { phone } = this.state;
    return (
      <div>
        <Consumer>
          {context => {
            console.log("context:", context);
            return (
              <div>
                <Input
                  placeholder="شماره تماس خود را وارد کنید."
                  onChange={this.handlePhoneNumber}
                  value={phone}
                />
                <button onClick={() => this.onSubmit(context)}>Moji</button>
              </div>
            );
          }}
        </Consumer>
      </div>
    );
  }
}

export default Login;
